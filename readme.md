# Requirements

* nodejs

## Install

```bash
npm install resumed jsonresume-theme-kendall
```

## Create a sample resume (will create the resume.json file)

```bash
npx resumed init
```

## Or Import it from your LinkedIn Profile using this Chrome extension (will create the resume.json file)

https://chrome.google.com/webstore/detail/json-resume-exporter/caobgmmcpklomkcckaenhjlokpmfbdec

## Render Resume  (create the resume.html file)

```bash
npx resumed render --theme kendall
```
